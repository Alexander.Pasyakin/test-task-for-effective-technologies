<?php
class Circle
{
    private $filename = "Circle.txt";
    
    public $params = array();

    public function area()
    {
        $this->params["area"] = pi() * pow($this->params["radius"], 2);
    }

    public function generateFigure()
    {
        $this->params["name"] = "Circle";
        $this->params["radius"] = rand();
    }

    public function saveObject()
    {
        $this->params = json_encode($this->params);
        file_put_contents($this->filename, $this->params);
    }

    public function getObject()
    {
        $this->params = file_get_contents($this->filename);
        $this->params = get_object_vars(json_decode($this->params));
    }
}
?>