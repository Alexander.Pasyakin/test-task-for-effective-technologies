<?php
class Fibonacci
{
    public function constructRow()
    {
        $rowFib = [0, 1];

        for($i = 2; $i < 65; $i++){
            $curr = $rowFib[$i - 1] + $rowFib[$i - 2];
            $rowFib[] = $curr;
        }

        Fibonacci::printRow($rowFib);
    }

    private function printRow($row)
    {
        foreach($row as $el){
            print($el . '; ');
        }
    }
}

Fibonacci::constructRow();
?>