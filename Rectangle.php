<?php
class Rectangle
{
    private $filename = "Rectangle.txt";

    public $params = array();

    public function area()
    {
        $this->params["area"] = $this->params["width"] * $this->params["height"];
    }

    public function generateFigure()
    {
        $this->params["name"] = "Rectangle";
        $this->params["width"] = rand();
        $this->params["height"] = rand();
    }

    public function saveObject()
    {
        $this->params = json_encode($this->params);
        file_put_contents($this->filename, $this->params);
    }

    public function getObject()
    {
        $this->params = file_get_contents($this->filename);
        $this->params = get_object_vars(json_decode($this->params));
    }
}
?>