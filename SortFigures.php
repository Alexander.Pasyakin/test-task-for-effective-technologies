<?php
require_once("Rectangle.php");
require_once("Triangle.php");
require_once("Circle.php");

$rectangle = new Rectangle;
$triangle = new Triangle;
$circle = new Circle;

$rectangle->generateFigure();
$rectangle->area();

$triangle->generateFigure();
$triangle->area();

$circle->generateFigure();
$circle->area();

$figures = array($rectangle->params, $triangle->params, $circle->params);

function sortFigures($f1, $f2)
{
    if($f1["area"] < $f2["area"]) return 1;
    elseif($f1["area"] > $f2["area"]) return -1;
    else return 0;
}

uasort($figures, "sortFigures");

function printArrayFigures($figures)
{
    $strout = "Sorted array:<br>";

    foreach($figures as $figure)
    {
        $strout .= $figure["name"] . ":<br>Area = " . $figure["area"] . "<br>";
    }
    print($strout);
}

printArrayFigures($figures);
?>