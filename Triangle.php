<?php
class Triangle
{
    private $filename = "Triangle.txt";
    
    public $params = array();

    public function area()
    {
        $this->params["area"] = ($this->params["base"] * $this->params["height"]) / 2;
    }

    public function generateFigure()
    {
        $this->params["name"] = "Triangle";
        $this->params["base"] = rand();
        $this->params["height"] = rand();
    }

    public function saveObject()
    {
        $this->params = json_encode($this->params);
        file_put_contents($this->filename, $this->params);
    }

    public function getObject()
    {
        $this->params = file_get_contents($this->filename);
        $this->params = get_object_vars(json_decode($this->params));
    }
}
?>